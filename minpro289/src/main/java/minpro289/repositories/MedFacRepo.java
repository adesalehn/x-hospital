package minpro289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import minpro289.models.M_medical_facility;

@Repository

public interface MedFacRepo extends JpaRepository<M_medical_facility, Long> {
	
	@Query(value = "SELECT * FROM m_medical_facility mf WHERE mf.medical_facility_category_id = ?1", nativeQuery = true)
	List<M_medical_facility> medFacByCatId(Long x);

}
