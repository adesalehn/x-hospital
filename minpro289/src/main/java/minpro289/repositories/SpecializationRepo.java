package minpro289.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import minpro289.models.M_specialization;

@Repository
public interface SpecializationRepo extends JpaRepository<M_specialization, Long> {
	
	@Query(value = "select id, name from M_specialization ms"
			+ " WHERE ms.Is_delete = false",nativeQuery = true)
	List <Map<String, Object>> listSpecializationAll();
	
	@Query(value = "FROM M_specialization ms WHERE ms.Is_delete = false ")
	List <M_specialization> listSpecialization();
	
	@Query(value = "SELECT * FROM m_specialization ms WHERE LOWER(ms.name) = LOWER(?1)", nativeQuery = true)
	List<M_specialization> checkData(String x);
	
	@Query(value = "SELECT * FROM m_specialization ms WHERE LOWER(ms.name) = LOWER(?1) AND ms.id <> ?2", nativeQuery = true)
	List<M_specialization> checkDataEdit(String x, int y);

	
	
	
	
	@Query(value = "SELECT * FROM m_specialization ms WHERE LOWER(ms.name) LIKE %:textcari%", nativeQuery = true)
	List<M_specialization> searchEngine(@Param("textcari") String textcari);
	
	
	
}
