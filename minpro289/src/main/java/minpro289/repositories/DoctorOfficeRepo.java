package minpro289.repositories;

import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import minpro289.models.T_doctor_office;

public interface DoctorOfficeRepo extends JpaRepository<T_doctor_office, Long> {
	@Query(value = "SELECT \r\n"
			+ "extract(year FROM min(tdo.created_on) ) AS tahun_awal \r\n"
			+ "FROM t_doctor_office_schedule tdo\r\n"
			+ "WHERE tdo.doctor_id = ?1" ,nativeQuery = true)
	Map<String, Object> listpengalaman(long id);

}
