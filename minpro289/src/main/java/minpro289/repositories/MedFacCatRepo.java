package minpro289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import minpro289.models.M_medical_facility_category;

@Repository

public interface MedFacCatRepo extends JpaRepository<M_medical_facility_category, Long> {
	@Query(value = "FROM M_medical_facility_category mfc WHERE mfc.Is_delete = false ")
	List <M_medical_facility_category> listMedFacCat();
	
	@Query(value = "SELECT * FROM M_medical_facility_category mfc WHERE LOWER(mfc.name) = LOWER(?1)", nativeQuery = true)
	List<M_medical_facility_category> checkData(String x);
	
	@Query(value = "SELECT * FROM m_medical_facility_category mfc WHERE LOWER(mfc.name) LIKE %:textcari% AND mfc.Is_delete = false", nativeQuery = true)
	List<M_medical_facility_category> searchEngine(@Param("textcari") String textcari);

}
