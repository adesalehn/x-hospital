package minpro289.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


import minpro289.models.M_education_level;

public interface DoctorEducationRepo extends JpaRepository<M_education_level, Long> {
	@Query(value = "select * from m_education_level where is_delete=false order by id",nativeQuery = true)
 	List<M_education_level> ListJenjang();
 	
 	@Query(value = "select * from m_education_level where id = ?1 ",nativeQuery = true)
 	M_education_level ListJenjangById(long id);
 	
 	
 	@Query(value = "select name from m_education_level where lower (name) = ?1 and is_delete=false",nativeQuery = true)
 	String carinamajenjang(String namajenjang);
 	
 	@Modifying //untuk insert delet dan update
	@Query(value = "insert into m_education_level(name,created_on,created_by,is_delete,deleted_by,modified_by)values(?1,now(),1,false,0,0)",nativeQuery = true)
    int InsertJenjang(String name); 
 	 
 	@Modifying
 	@Query(value="update m_education_level set name=?1, modified_on = now() where id=?2 ",nativeQuery = true)
 	int UpdateJenjang(String name,long id);
 	
 	@Modifying
 	@Query(value="update m_education_level set is_delete=true,deleted_by = 1,deleted_on= now() where id=?1 ",nativeQuery = true)
 	int DeleteJenjang(long id);
 	 	
 	@Query(value = "select id from m_education_level where lower(name) = ?1",nativeQuery = true)
	Long cariid(String name);


}
