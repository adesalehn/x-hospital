package minpro289.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import minpro289.models.M_doctor;

public interface DoctorDetailRepo extends JpaRepository<M_doctor, Long> {
	
	@Query(value =	"SELECT\r\n"
			+ "de.institution_name,\r\n"
			+ "de.end_year,\r\n"
			+ "de.major\r\n"
			+ "FROM m_doctor d\r\n"
			+ "JOIN m_doctor_education de on d.id=de.doctor_id\r\n"
			+ "WHERE d.id = ?1",nativeQuery= true)
	List<Map<String, Object>> pendidikan(long id);
	
	@Query(value = "SELECT\r\n"
			+ "extract(year from tdo.created_on) AS tahun_awal,  \r\n"
			+ "extract(year from tdo.deleted_on) AS tahun_akhir,  \r\n"
			+ "mdf.name, \r\n"
			+ "tdo.specialization, \r\n"
			+ "mll.name AS lokasi, \r\n"
			+ "tdo.is_delete  \r\n"
			+ "FROM t_doctor_office tdo  \r\n"
			+ "JOIN m_medical_facility mdf on tdo.medical_facility_id = mdf.id  \r\n"
			+ "JOIN m_location ml on mdf.location_id = ml.id  \r\n"
			+ "JOIN m_location_level mll on ml.location_level_id = mll.id  \r\n"
			+ "WHERE tdo.doctor_id = ?1 AND mdf.id<>5 \r\n"
			+ "ORDER BY tahun_awal DESC",nativeQuery = true)
	List<Map<String, Object>> listriwayat(long doctor_id);
	
	@Query(value = "SELECT\r\n"
			+ "s.name,\r\n"
			+ "b.fullname,\r\n"
			+ "b.image_path\r\n"
			+ "FROM m_doctor d\r\n"
			+ "JOIN m_biodata b ON d.biodata_id = b.id\r\n"
			+ "JOIN t_current_doctor_specialization cd ON cd.doctor_id = d.id\r\n"
			+ "JOIN m_specialization s ON s.id= cd.specialization_id\r\n"
			+ "WHERE b.id = ?1",nativeQuery= true)
	Map<String, Object> nama(long id);
	
	@Query(value = "SELECT dt.name\r\n"
			+ "FROM m_doctor d \r\n"
			+ "JOIN t_doctor_treatment dt on d.id = dt.doctor_id\r\n"
			+ "WHERE d.id = ?1",nativeQuery= true)
	List<Map<String, Object>> tindakan(long id);
	
	@Query(value = "SELECT \r\n"
			+ "mll.name AS kab, \r\n"
			+ "ml.name AS kec, \r\n"
			+ "dotp.price, \r\n"
			+ "tdo.specialization, \r\n"
			+ "mf.name, \r\n"
			+ "mf.id,\r\n"
			+ "mf.full_address,\r\n"
			+ "mfs.day, \r\n"
			+ "mfs.time_schedule_start,\r\n"
			+ "mfs.time_schedule_end \r\n"
			+ "FROM m_doctor md\r\n"
			+ "JOIN t_doctor_office tdo ON tdo.doctor_id = md.id\r\n"
			+ "JOIN m_medical_facility mf ON mf.id = tdo.medical_facility_id\r\n"
			+ "JOIN m_medical_facility_schedule mfs ON mf.id=mfs.medical_facility_id\r\n"
			+ "JOIN t_doctor_office_treatment dot ON dot.doctor_office_id= tdo.id\r\n"
			+ "JOIN t_doctor_office_treatment_price dotp ON dotp.doctor_office_treatment_id = dot.id\r\n"
			+ "JOIN m_location ml ON ml.id=mf.location_id\r\n"
			+ "JOIN m_location_level mll ON mll.id=ml.location_level_id\r\n"
			+ "WHERE tdo.is_delete = false AND mf.id <> 5 AND md.id=?1",nativeQuery= true)
	List<Map<String, Object>> listdetail(long id);
	
	@Query(value = "SELECT *,\r\n"
			+ "CASE\r\n"
			+ "	WHEN\r\n"
			+ "	(SELECT count(1) \r\n"
			+ "		FROM m_medical_facility_schedule mmfsh \r\n"
			+ "	 	JOIN t_doctor_office_schedule tdos ON tdos.medical_facility_schedule_id = mmfsh.id \r\n"
			+ " 		WHERE tdos.doctor_id = jadwal.doctor_id AND mmfsh.medical_facility_id = 5 AND mmfsh.day=current_day_indonesia \r\n"
			+ " 		AND (TO_TIMESTAMP(to_char(now(),'HH24:MI'),'HH24:MI') \r\n"
			+ "	  	BETWEEN TO_TIMESTAMP(mmfsh.time_schedule_start,'HH24:MI') \r\n"
			+ "	  	and TO_TIMESTAMP(mmfsh.time_schedule_end,'HH24:MI')))>0 then true else false end AS is_online		 \r\n"
			+ "	FROM\r\n"
			+ "(SELECT \r\n"
			+ "tdos.doctor_id,\r\n"
			+ "mll.name AS kab,   \r\n"
			+ "mfs.day,  \r\n"
			+ "mfs.time_schedule_start,  \r\n"
			+ "mfs.time_schedule_end,\r\n"
			+ "	CASE \r\n"
			+ "	WHEN EXTRACT(dow FROM now()) = 1 THEN 'Senin' \r\n"
			+ "	WHEN EXTRACT(dow FROM now()) = 2 THEN 'Selasa'\r\n"
			+ "	WHEN EXTRACT(dow FROM now()) = 3 THEN 'Rabu'\r\n"
			+ "	WHEN EXTRACT(dow FROM now()) = 4 THEN 'Kamis'\r\n"
			+ "	WHEN EXTRACT(dow FROM now()) = 5 THEN 'Jumat'\r\n"
			+ "	WHEN EXTRACT(dow FROM now()) = 6 THEN 'Sabtu'\r\n"
			+ "	WHEN EXTRACT(dow FROM now()) = 7 THEN 'Minggu'\r\n"
			+ "	END AS current_day_indonesia\r\n"
			+ "	  FROM m_doctor md    \r\n"
			+ "		  JOIN t_doctor_office dof ON dof.doctor_id = md.id    \r\n"
			+ "		  JOIN t_doctor_office_schedule tdos ON tdos.doctor_id = dof.doctor_id \r\n	"		
			+ "		  JOIN m_medical_facility mf ON mf.id = dof.medical_facility_id    \r\n"
			+ "		  JOIN m_medical_facility_schedule mfs ON mf.id = mfs.medical_facility_id  \r\n"
			+ "		  JOIN t_doctor_office_treatment dot ON dot.doctor_office_id = dof.id    \r\n"
			+ "		  JOIN t_doctor_office_treatment_price dotq ON dotq.doctor_office_treatment_id = dot.id   \r\n"
			+ "		  JOIN m_location ml ON ml.id = mf.location_id   \r\n"
			+ "		  JOIN m_location_level mll ON mll.id = ml.location_level_id   \r\n"
			+ "		  WHERE dof.is_delete = false AND md.id = ?1)AS jadwal",nativeQuery= true)
	List<Map<String, Object>> listdetailonline(long id);
	
//	@Query(value = "SELECT \r\n"
//			+ "tdo.doctor_id,\r\n"
//			+ "mmfs.day,\r\n"
//			+ "mmfs.time_schedule_start,\r\n"
//			+ "mmfs.time_schedule_end,\r\n"
//			+ "mmf.name \r\n"
//			+ "FROM m_doctor md  \r\n"
//			+ "JOIN t_doctor_office tdo ON tdo.doctor_id = md.id  \r\n"
//			+ "JOIN m_medical_facility mmf ON mmf.id = tdo.medical_facility_id  \r\n"
//			+ "JOIN m_medical_facility_schedule mmfs ON mmf.id=mmfs.medical_facility_id \r\n"
//			+ "JOIN t_doctor_office_treatment dot ON dot.doctor_office_id= tdo.id  \r\n"
//			+ "JOIN t_doctor_office_treatment_price dotp ON dotp.doctor_office_treatment_id = dot.id \r\n"
//			+ "WHERE tdo.is_delete = false AND mmf.id = ?1",nativeQuery= true)
//	List<Map<String, Object>> waktudetail(long idRS);



}
