package minpro289.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import minpro289.repositories.DoctorDetailRepo;
import minpro289.repositories.DoctorOfficeRepo;


@Service
@Transactional
public class DetailDokterService {

	@Autowired
	private DoctorDetailRepo dokde;
	
	public List<Map<String, Object>> listdetailonline(long id) {
		return dokde.listdetailonline(id);
	}
	
	public List<Map<String, Object>> listdetail(long id) {
		return dokde.listdetail(id);
	}
	
//	public List<Map<String, Object>> waktudetail(long idRS) {
//		return dokde.waktudetail(idRS);
//	}
	
	public List<Map<String, Object>> tindakan(long id) {
		return dokde.tindakan(id);
	}
	
	public List<Map<String, Object>> pendidikan(long id) {
		return dokde.pendidikan(id);
	}
	
	public List<Map<String, Object>> riwayat(long doctor_id) {
		return dokde.listriwayat(doctor_id);
	}
	
	public Map<String, Object> nama(long id) {
		return dokde.nama(id);
	}
	
	@Autowired DoctorOfficeRepo dor;
	public Map<String, Object> pengalaman(long id) {
		return dor.listpengalaman(id);
	}

}
