package minpro289.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import minpro289.repositories.DoctorTreatmentRepo;
import minpro289.repositories.LocationLevelRepo;
import minpro289.repositories.SpecializationRepo;

@Service
@Transactional
public class CariDokterService {
	
	@Autowired
	private LocationLevelRepo llRepo;
	
	public List<Map<String, Object>> listLocation(){
		return llRepo.listLocation();
	}
	
	@Autowired
	private SpecializationRepo specRepo;
	
	public List<Map<String, Object>> listSpecialization() {
		return specRepo.listSpecializationAll();
	}
	
	@Autowired
	private DoctorTreatmentRepo doctreatRepo;
	
	public List<Map<String, Object>> listTreatment(){
		return doctreatRepo.listTreatment();
	}

}
