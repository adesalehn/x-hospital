package minpro289.services;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import minpro289.repositories.DoctorOfficeScheduleRepo;

@Service
@Transactional
public class CariDokterHasilService {

	@Autowired
	private DoctorOfficeScheduleRepo dosRepo;
	
	public List<Map<String, Object>> allDoctorSchedule(long id) {
		return dosRepo.allDoctorSchedule(id);
	}
	
	public List<Map<String, Object>> searchResult(String lokasi, String fullname, String spesialis, String tindakan) {
		return dosRepo.searchResult(lokasi, fullname, spesialis, tindakan);
	}
	
}
