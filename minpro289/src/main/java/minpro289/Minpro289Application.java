package minpro289;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Minpro289Application {

	public static void main(String[] args) {
		SpringApplication.run(Minpro289Application.class, args);
	}
}
