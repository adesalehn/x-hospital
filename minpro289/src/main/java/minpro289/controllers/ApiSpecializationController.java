package minpro289.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import minpro289.models.M_specialization;
import minpro289.models.T_current_doctor_specialization;
import minpro289.repositories.CurrentDoctorSpecRepo;
import minpro289.repositories.SpecializationRepo;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/api")
public class ApiSpecializationController {
	
	@Autowired SpecializationRepo specrepo;
	
	@GetMapping(value="/spec")
	public ResponseEntity<List<M_specialization>> getAllSpec(){
		try {
			List<M_specialization> m_specialization = this.specrepo.listSpecialization();
			return new ResponseEntity<List<M_specialization>>(m_specialization, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<M_specialization>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("spec/{id}")
	public ResponseEntity<?> getSpecById(@PathVariable Long id){
		try {
			M_specialization m_specialization = this.specrepo.findById(id).orElse(null);
			if(m_specialization != null) {
				return new ResponseEntity<M_specialization>(m_specialization,HttpStatus.OK);
			}
			else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Specialization dengan ID "+id+" tidak ditemukan");
			}
		}
		catch (Exception e) {
			return new ResponseEntity<M_specialization>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/spec")
	public ResponseEntity<M_specialization> insertSpecialization(@RequestBody M_specialization m_specialization) {
		try {
			m_specialization.setCreated_by((long) 1);
			m_specialization.setCreated_on(LocalDateTime.now());
			this.specrepo.save(m_specialization);
			return new ResponseEntity<M_specialization>(m_specialization,HttpStatus.OK);
		} 
		catch(Exception e) {
			return new ResponseEntity<M_specialization>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/spec/{id}")
	public ResponseEntity<M_specialization>editSpecialization(@RequestBody M_specialization m_specialization, 
			@PathVariable Long id){
		try {
			m_specialization.setId(id);
//			m_specialization.setCreated_by((long) 1);
//			m_specialization.setCreated_on(LocalDateTime.now());
			this.specrepo.save(m_specialization);
			return new ResponseEntity<M_specialization>(m_specialization,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<M_specialization>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("checkspec/{check}")
	public ResponseEntity<?> checkData(@PathVariable ("check") String check){
		try {
			List<M_specialization> m_specialization = this.specrepo.checkData(check);
			return new ResponseEntity<List<M_specialization>>(m_specialization, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<M_specialization>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("checkspecEdit/{check}/{id}")
	public ResponseEntity<?> checkDataedit(@PathVariable ("check") String check, @PathVariable ("id") int id){
		try {
			List<M_specialization> m_specialization = this.specrepo.checkDataEdit(check, id);
			return new ResponseEntity<List<M_specialization>>(m_specialization, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<M_specialization>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/searchdata/{search}")
	public ResponseEntity<?> searchdata(@PathVariable ("search") String search){
		try {
			List<M_specialization> m_specialization = this.specrepo.searchEngine(search);
			return new ResponseEntity<List<M_specialization>>(m_specialization, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<M_specialization>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@Autowired CurrentDoctorSpecRepo cdsrepo;
//	@GetMapping(value="/specdoc")
//	public ResponseEntity<List<T_current_doctor_specialization>> getAllSpecdoc(){
//		try {
//			List<T_current_doctor_specialization> m_specialization = this.cdsrepo.findAll();
//			return new ResponseEntity<List<T_current_doctor_specialization>>(m_specialization, HttpStatus.OK);
//		}
//		catch (Exception E) {
//			return new ResponseEntity<List<T_current_doctor_specialization>>(HttpStatus.NO_CONTENT);
//		}
//	}
	@GetMapping("/specdata/{id}")
	public ResponseEntity<?> searchdatabro(@PathVariable Long id){
		try {
			List<T_current_doctor_specialization> cds = this.cdsrepo.checkDataDelete(id);
			return new ResponseEntity<List<T_current_doctor_specialization>>(cds, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<T_current_doctor_specialization>>(HttpStatus.NO_CONTENT);
		}
	}
	
	
}
