package minpro289.controllers;

import java.time.LocalDateTime;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import minpro289.models.M_specialization;
import minpro289.repositories.SpecializationRepo;

@Controller
@RequestMapping(value="/m_specialization/")
public class SpecializationController {
	
	@Autowired
	private SpecializationRepo specrepo;
	
	@GetMapping(value="index")
	public ModelAndView index(){
		ModelAndView view = new ModelAndView("/m_specialization/index");
		List<M_specialization> listspec = this.specrepo.findAll();
		view.addObject("listspec",listspec);
		return view;
		
	}
	
	@GetMapping(value="indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("/m_specialization/indexapi");
		return view;
		
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view  = new ModelAndView("/m_specialization/form");
		M_specialization m_specialization = new M_specialization();
		view.addObject("m_specialization", m_specialization);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute M_specialization m_specialization, BindingResult result) {
		if (!result.hasErrors()) {
			m_specialization.setCreated_by((long) 1);
			m_specialization.setCreated_on(LocalDateTime.now());
			this.specrepo.save(m_specialization);
		}
		return new ModelAndView("redirect:/m_specialization/index");
	}
		
	@GetMapping(value="/edit/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view  = new ModelAndView("/m_specialization/form");
		M_specialization m_specialization = this.specrepo.findById(id).orElse(null);
		view.addObject("m_specialization", m_specialization);
		return view;
		
	}
	
	@GetMapping(value="/deleteform/{id}")
	public ModelAndView deleteform(@PathVariable("id") Long id) {
		ModelAndView view  = new ModelAndView("/m_specialization/deleteform");
		M_specialization m_specialization = this.specrepo.findById(id).orElse(null);
		view.addObject("m_specialization", m_specialization);
		return view;
	}
	
	@GetMapping(value="/del/{id}")
	public ModelAndView del(@PathVariable("id") Long id) {
		if (id != null) {
			this.specrepo.deleteById(id);
		}
		return new ModelAndView("redirect:/m_specialization/index");
	}
}
