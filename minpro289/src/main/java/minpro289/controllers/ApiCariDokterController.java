package minpro289.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import minpro289.services.CariDokterService;

@RestController
@RequestMapping(value="/api/modalcaridokter")

public class ApiCariDokterController {

	@Autowired 
	private CariDokterService cds;

	@GetMapping(value = "/lokasi")
	public List<Map<String, Object>>listLocation(){
		return cds.listLocation();
	}
	
	@GetMapping(value = "/specialization")
	public List<Map<String, Object>>listSpecialization(){
		return cds.listSpecialization();
	}
	
	@GetMapping(value = "/treatment")
	public List<Map<String, Object>>listTreatment(){
		return cds.listTreatment();
	}
}
