package minpro289.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/detail_dokter")
public class DetailDokterController {
	
	@GetMapping(value="/index")
	public ModelAndView hasilCari() {
		ModelAndView view = new ModelAndView("/detail_dokter/index");
		return view;
	}

}
