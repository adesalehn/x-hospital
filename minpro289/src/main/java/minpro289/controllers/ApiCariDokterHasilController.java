package minpro289.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import minpro289.services.CariDokterHasilService;

@RestController
@RequestMapping(value="/api/caridokter")
public class ApiCariDokterHasilController {

	@Autowired
	private CariDokterHasilService dos_s;
	
	@GetMapping(value = "alldoctorschedule/{id}")
	public List<Map<String, Object>>allDoctorSchedule(@PathVariable long id){
		return dos_s.allDoctorSchedule(id);
	}
	
	@GetMapping("searchresult/{lokasi}/{fullname}/{spesialis}/{tindakan}")
	public List<Map<String, Object>>searchResult(@PathVariable String lokasi, @PathVariable String fullname,
			@PathVariable String spesialis, @PathVariable String tindakan){
		lokasi=lokasi.equalsIgnoreCase("kosong")?"":lokasi;
		fullname=fullname.equalsIgnoreCase("kosong")?"":fullname;
		spesialis=spesialis.equalsIgnoreCase("kosong")?"":spesialis;
		tindakan=tindakan.equalsIgnoreCase("kosong")?"":tindakan; 
		return dos_s.searchResult(lokasi, fullname, spesialis, tindakan);
	}
	
	
}
