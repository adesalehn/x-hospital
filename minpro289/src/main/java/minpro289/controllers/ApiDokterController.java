package minpro289.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import minpro289.services.DetailDokterService;

@RestController
@RequestMapping(value = "api/detail/")
public class ApiDokterController {

	@Autowired
	private DetailDokterService dds;
	
	@GetMapping(value ="online/{id}" )
	public List<Map<String, Object>>listdetailonline(@PathVariable long id){
		return dds.listdetailonline(id);
	}
	 
	@GetMapping(value ="lokasi/{id}" )
	public List<Map<String, Object>>listdetail(@PathVariable long id){
		return dds.listdetail(id);
	}
	 
	@GetMapping(value ="tindakan/{id}" )
	public List<Map<String, Object>>tindakan(@PathVariable long id){
		return dds.tindakan(id);
	}
	 
	@GetMapping(value ="pendidikan/{id}" )
	public List<Map<String, Object>>pendidikan(@PathVariable long id){
		return dds.pendidikan(id);
	}
	
	@GetMapping(value ="nama/{id}" ) 
	public Map<String, Object>nama(@PathVariable long id){
		return dds.nama(id);
	}
	 
//	@GetMapping(value ="waktu/{idRS}" )
//	public List<Map<String, Object>>waktu(@PathVariable long idRS){
//		return dds.waktudetail(idRS);
//	}
	
	@GetMapping(value ="riwayat/{doctor_id}" )
	public List<Map<String, Object>>riwayat(@PathVariable long doctor_id){
		return dds.riwayat(doctor_id);
	}	
	
	@GetMapping(value ="pengalaman/{doctor_id}" )
	public Map<String, Object>pengalaman(@PathVariable long doctor_id){
		return dds.pengalaman(doctor_id);
	}
	
}