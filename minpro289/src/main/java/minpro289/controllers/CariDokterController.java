package minpro289.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/cari_dokter")
public class CariDokterController {
	
	@GetMapping(value="/hasil_cari_dokter")
	public ModelAndView hasilCari() {
		ModelAndView view = new ModelAndView("/cari_dokter/hasil_cari_dokter");
		return view;
	}
}
