package minpro289.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import minpro289.models.M_medical_facility;
import minpro289.models.M_medical_facility_category;
import minpro289.repositories.MedFacCatRepo;
import minpro289.repositories.MedFacRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMedFacCatController {
	
	@Autowired
	private MedFacCatRepo mfcrepo;
	
	@GetMapping(value="/medfc")
	public ResponseEntity<List<M_medical_facility_category>> getAllSpec(){
		try {
			List<M_medical_facility_category> m_medical_facility_category = this.mfcrepo.listMedFacCat();
			return new ResponseEntity<List<M_medical_facility_category>>(m_medical_facility_category, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<M_medical_facility_category>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("medfc/{id}")
	public ResponseEntity<?> getSpecById(@PathVariable Long id){
		try {
			M_medical_facility_category m_medical_facility_category = this.mfcrepo.findById(id).orElse(null);
			if(m_medical_facility_category != null) {
				return new ResponseEntity<M_medical_facility_category>(m_medical_facility_category,HttpStatus.OK);
			}
			else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Specialization dengan ID "+id+" tidak ditemukan");
			}
		}
		catch (Exception e) {
			return new ResponseEntity<M_medical_facility_category>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/medfc")
	public ResponseEntity<M_medical_facility_category> insertSpecialization(@RequestBody M_medical_facility_category m_medical_facility_category) {
		try {
			m_medical_facility_category.setCreated_by((long) 1);
			m_medical_facility_category.setCreated_on(LocalDateTime.now());
			this.mfcrepo.save(m_medical_facility_category);
			return new ResponseEntity<M_medical_facility_category>(m_medical_facility_category,HttpStatus.OK);
		} 
		catch(Exception e) {
			return new ResponseEntity<M_medical_facility_category>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/medfc/{id}")
	public ResponseEntity<M_medical_facility_category>editSpecialization(@RequestBody M_medical_facility_category m_medical_facility_category, 
			@PathVariable Long id){
		try {
			m_medical_facility_category.setId(id);
//			m_medical_facility_category.setCreated_by((long) 1);
//			m_medical_facility_category.setCreated_on(LocalDateTime.now());
			this.mfcrepo.save(m_medical_facility_category);
			return new ResponseEntity<M_medical_facility_category>(m_medical_facility_category,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<M_medical_facility_category>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("checkmedfc/{check}")
	public ResponseEntity<?> checkData(@PathVariable ("check") String check){
		try {
			List<M_medical_facility_category> m_medical_facility_category = this.mfcrepo.checkData(check);
			return new ResponseEntity<List<M_medical_facility_category>>(m_medical_facility_category, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<M_medical_facility_category>>(HttpStatus.NO_CONTENT);
		}
	}
	
//	@GetMapping("/searchdatamed/{search}")
//	public ResponseEntity<?> searchdatamed(@PathVariable ("search") String search){
//		try {
//			List<M_medical_facility_category> listmed = this.mfcrepo.findAll();
//			List<M_medical_facility_category> listFilter = new ArrayList<M_medical_facility_category>();
//			listFilter = (List<M_medical_facility_category>) listmed.stream().filter(x ->{
//				//System.out.print("Filter Category " + x);
//				return x.getName().equalsIgnoreCase(search);
//			}).collect(Collectors.toList());
//			return new ResponseEntity<>(listFilter, HttpStatus.OK);		
//			}
//		catch (Exception E) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		}
//	}
//	
	@Autowired
	private MedFacRepo mfrepo;
	@GetMapping("/findbycatid/{id}")
	public ResponseEntity<?> searchdatabro(@PathVariable Long id){
		try {
			List<M_medical_facility> cds = this.mfrepo.medFacByCatId(id);
			return new ResponseEntity<List<M_medical_facility>>(cds, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<M_medical_facility>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("filter")
	public ResponseEntity<List<M_medical_facility_category>> listFilter(){
		try {
			List<M_medical_facility_category> listmed = this.mfcrepo.findAll();
			List<M_medical_facility_category> listFilter = new ArrayList<M_medical_facility_category>();
			listFilter = (List<M_medical_facility_category>) listmed.stream().filter(x ->x.getName().equals("name")).collect(Collectors.toList());
			return new ResponseEntity<>(listFilter, HttpStatus.OK);
		}
		catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);//sourcetree
			
		}
	}
	
	
	
	
	@GetMapping("/searchdatamed/{search}")
	public ResponseEntity<?> searchdatamed(@PathVariable ("search") String search){ //search with query customize
		try {
			List<M_medical_facility_category> m_medical_facility_category = this.mfcrepo.searchEngine(search);
			return new ResponseEntity<List<M_medical_facility_category>>(m_medical_facility_category, HttpStatus.OK);
		}
		catch (Exception E) {
			return new ResponseEntity<List<M_medical_facility_category>>(HttpStatus.NO_CONTENT);
		}
	}
//	@GetMapping("/sortacs")
//    public ResponseEntity<List<M_medical_facility_category>> listmed(){
//        try {
//            List<M_medical_facility_category> listmedic = this.mfcrepo.findAll();
//            List<M_medical_facility_category> sortpin = new ArrayList<M_medical_facility_category>();
//            sortpin = (List<M_medical_facility_category>) listmedic.stream().sorted((p, q) -> p.getName().compareTo(q.getName())).collect(Collectors.toList());
//            return new ResponseEntity<>(sortpin, HttpStatus.OK);
//        } catch (Exception e) {
//            //TODO: handle exception
//            e.printStackTrace();
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }

}
