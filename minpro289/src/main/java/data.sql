-- m_biodata 
insert into m_biodata(fullname, mobile_phone, image_path, created_by, created_on, is_delete)
values 
('dr. Amelia Suganda, Sp.OG', '081315139988', '/image/amelia.png', 1, now(), false),
('dr. Putu Aditya Wiguna, M.Sc, Sp.A', '081315019988', '/image/putu.png', 1, now(), false),
('dr. Ardorisye Saptaty Fornia, Sp.P, M.Kes', '081315109988', '/image/fornia.jpg', 1, now(), false),
('dr. Christopher Warouw, Sp.T.H.T.K.L', '081315139980', '/image/warrow.jpg', 1, now(), false),
('dr. Djonggi P. Panggabean, Sp.M(K)', '081315139988', '/image/pangga.jpg', 1, now(), false),
('dr. Klara Yuliarti, Sp.A (K)', '081315139988', '/image/klara.jpg', 1, now(), false),
('dr. Ivan Riyanto Widjaja, Sp.A', '081315139988', '/image/ivan.jpg', 1, now(), false),
('Prof. DR. dr. Rini Sekartini, Sp.A (K)', '081315139988', '/image/rini.jpg', 1, now(), false),
('dr. Dimple Gobind Nagrani, Sp.A', '081315139988', '/image/dimple.jpg', 1, now(), false),
('dr. Finny Fitry Yani, Sp.A (K)', '081315139988', '/image/finny.jpg', 1, now(), false),
('dr. Ade Permana, Sp.OG-KFER', '081315139988', '/image/dea.jpg', 1, now(), false),
('dr. Liva Wijaya, Sp.OG', '081315139988', '/image/liva.jpg', 1, now(), false),
('Ade Saleh Nurrohim', '081315139128', '', 1, now(), false),
('Bagus Aryo Nugroho', '081715139128', '', 1, now(), false),
('Chanron Sitanggang', '081315130128', '', 1, now(), false);

-- m_user 
insert into m_user(created_by, created_on, biodata_id) values 
(99, now(), 13);

-- m_doctor
insert into m_doctor(biodata_id, str, created_by, created_on, is_delete)
values (1, 'Mrs. Amelia', 1, now(), false),
(2, 'Mr. Putu', 1, now(), false),
(3, 'Mrs. Fornia', 1, now(), false),
(4, 'Mr. Warrow', 1, now(), false),
(5, 'Mr. Bean', 1, now(), false),
(6, 'Mrs. Klara', 1, now(), false),
(7, 'Mr. Ivan', 1, now(), false),
(8, 'Mrs. Rini', 1, now(), false),
(9, 'Mrs. Dimple', 1, now(), false),
(10, 'Mrs. Finny', 1, now(), false),
(11, 'Mr. Ade', 1, now(), false),
(12, 'Mrs. Liva', 1, now(), false);

--m_education_level
insert into m_education_level(name, created_by, created_on, is_delete)
values 
('Bachelor', 1, now(), false),
('Master', 1, now(), false),
('Doctor', 1, now(), false);

--m_doctor_education
insert into m_doctor_education(doctor_id, education_level_id, institution_name, major, start_year, end_year, is_last_education, created_by, created_on, is_delete)
values 
(1, 2, 'Universitas Airlangga', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(1, 2, 'Universitas Gajah Mada', 'Spesialis Obstetri & Ginekologi', '2010', '2012', false, 1, now(), false),
(1, 1, 'Universitas Indonesia', 'Sarjana Kedokteran', '2006', '2010', false, 1, now(), false),
(2, 2, 'Universitas Gajah Mada', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(3, 3, 'Universitas Budi Luhur', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(4, 2, 'Universitas Telkom', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(5, 2, 'Universitas Pamulang', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(6, 2, 'Universitas Padjajaran', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(7, 2, 'Universitas Padjajaran', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(8, 2, 'Universitas Padjajaran', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(9, 2, 'Universitas Padjajaran', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(10, 2, 'Universitas Padjajaran', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(11, 2, 'Universitas Padjajaran', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false),
(12, 2, 'Universitas Padjajaran', 'Magister Administrasi Rumah Sakit', '2016', '2020', false, 1, now(), false);

--m_specialization
insert into m_specialization(name, created_by, created_on, is_delete)
values ('Spesialis Anak', 1, now(), false),
('Spesialis Kandungan', 1, now(), false),
('Spesialis Mata', 1, now(), false),
('Spesialis Paru-paru', 1, now(), false),
('Spesialis THT', 1, now(), false);

--t_current_doctor_specialization
insert into t_current_doctor_specialization(doctor_id, specialization_id, created_by, created_on, is_delete)
values
(1, 2, 1, now(), false),
(2, 1, 1, now(), false),
(3, 4, 1, now(), false),
(4, 5, 1, now(), false),
(5, 3, 1, now(), false),
(6, 1, 1, now(), false),
(7, 1, 1, now(), false),
(8, 1, 1, now(), false),
(9, 1, 1, now(), false),
(10, 1, 1, now(), false),
(11, 2, 1, now(), false),
(12, 2, 1, now(), false);

--t_doctor_treatment
insert into t_doctor_treatment (doctor_id, name, created_by, created_on, is_delete)
values 
(1, 'Pengendalian kehamilan', 1, now(), false),
(1, 'Menopause', 1, now(), false),
(1, 'Infeksi saluran kemih', 1, now(), false),
(1, 'Masalah pada panggul', 1, now(), false),
(1, 'Infeksi kelamin', 1, now(), false),
(2, 'Tumbuh kembang anak', 1, now(), false),
(2, 'Pertumbuhan anak', 1, now(), false),
(2, 'Pernafasan anak', 1, now(), false),
(3, 'Treatment paru-paru', 1, now(), false),
(3, 'Penanganan covid-19', 1, now(), false),
(3, 'Sulit bernafas', 1, now(), false),
(4, 'Gangguan pendengaran', 1, now(), false),
(4, 'Infeksi saluran telinga', 1, now(), false),
(4, 'Telinga berdengung', 1, now(), false),
(5, 'Infeksi mata', 1, now(), false),
(5, 'Gangguan pengelihatan', 1, now(), false),
(5, 'Kelainan kornea', 1, now(), false),
(6, 'Tumbuh kembang anak', 1, now(), false),
(6, 'Pertumbuhan anak', 1, now(), false),
(6, 'Pernafasan anak', 1, now(), false),
(7, 'Tumbuh kembang anak', 1, now(), false),
(7, 'Pertumbuhan anak', 1, now(), false),
(7, 'Pernafasan anak', 1, now(), false),
(8, 'Tumbuh kembang anak', 1, now(), false),
(8, 'Pertumbuhan anak', 1, now(), false),
(8, 'Pernafasan anak', 1, now(), false),
(9, 'Tumbuh kembang anak', 1, now(), false),
(9, 'Pertumbuhan anak', 1, now(), false),
(9, 'Pernafasan anak', 1, now(), false),
(10, 'Tumbuh kembang anak', 1, now(), false),
(10, 'Pertumbuhan anak', 1, now(), false),
(10, 'Pernafasan anak', 1, now(), false),
(11, 'Pengendalian kehamilan', 1, now(), false),
(11, 'Menopause', 1, now(), false),
(11, 'Infeksi saluran kemih', 1, now(), false),
(12, 'Pengendalian kehamilan', 1, now(), false),
(12, 'Menopause', 1, now(), false),
(12, 'Infeksi saluran kemih', 1, now(), false);

--m_location_level
insert into m_location_level (name, created_on, created_by, is_delete)
values 
('Jakarta Pusat', '2020-07-25 1:00:00', 1, false),
('Jakarta Timur', now(), 1, false),
('Jakarta Selatan', now(), 1, false),
('Jakarta Utara', now(), 1, false),
('Jakarta Barat', now(), 1, false),
('Depok', now(), 1, false),
('Bandung', now(), 1, false),
('Flexible', now(), 1, false);

--m_location
insert into m_location (name, location_level_id, created_on, created_by, is_delete)
values 
('Pasar Senen', 1, now(), 1, false),
('Blok M', 3, now(), 1, false),
('Senopati', 3, now(), 1, false),
('Srengseng', 5, now(), 1, false),
('Meruya', 5, now(), 1, false),
('Dago', 7, now(), 1, false),
('Margonda', 6, now(), 1, false),
('Kebayoran Lama', 3, now(), 1, false),
('Rumah', 8, now(), 1, false);


--m_medical_facility_category
insert into m_medical_facility_category (name, created_on, created_by, is_delete)
values 
('Rumah Sakit', now(), 1, false),
('Klinik', now(), 1, false),
('Apotek', now(), 1, false),
('Online', now(), 1, false);

--m_medical_facility
insert into m_medical_facility (name, medical_facility_category_id, location_id, full_address, email, phone_code, phone, fax, created_on, created_by, is_delete)
values 
('RS Mitra', 1, 7, 'Jl. Pahlawan no.45', 'admin@rsmitra.com', '+62', '14022', '-', now(), 1, false),
('RSIA Bunda', 1, 1, 'Jl. Monas no.45', 'admin@rsiabunda.com', '+62', '14045', '-', now(), 1, false),
('RSUP Hasan Sadikin', 1, 6, 'Jl. Gedung Sate no.45', 'admin@rsuphasansadikin.com', '+62', '911', '-', now(), 1, false),
('RS Persahabatan', 1, 8, 'Jl. Langsat No.1', 'admin@rspersahabatan.com', '+62', '928232', '-', now(), 1, false),
('Layanan Online', 4, 9, 'Rumah Dokter Masing-masing', 'admin@online.com', '+62', '928232', '-', now(), 1, false);

--t_docter_office 
insert into t_doctor_office (doctor_id, medical_facility_id, specialization, created_on, created_by, deleted_on, deleted_by, is_delete)
values 
(1, 3, 'Dokter Kandungan', '2016-01-01', 1, '2018-01-01', 1, true),
(1, 2, 'Dokter Kandungan', '2018-01-01', 1, null, null, false),
(1, 1, 'Dokter Kandungan', '2019-01-01', 1, null, null, false),
(2, 4, 'Dokter Anak', '2013-01-01', 1, '2018-01-01', 1, true),
(2, 2, 'Dokter Anak', '2019-01-01', 1, null, null, false),
(2, 3, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(3, 3, 'Dokter Paru-paru', '2011-01-01', 1, '2018-01-01', 1, true),
(3, 1, 'Dokter Paru-paru', '2019-01-01', 1, null, null, false),--inilayananchat
(1, 5, 'Dokter Kandungan', '2016-01-01', 1, '2018-01-01', 1, true),
(1, 5, 'Dokter Kandungan', '2018-01-01', 1, null, null, false),
(1, 5, 'Dokter Kandungan', '2019-01-01', 1, null, null, false),
(2, 5, 'Dokter Anak', '2013-01-01', 1, '2018-01-01', 1, true),
(2, 5, 'Dokter Anak', '2019-01-01', 1, null, null, false),
(2, 5, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(3, 5, 'Dokter Paru-paru', '2011-01-01', 1, '2018-01-01', 1, true),
(3, 5, 'Dokter Paru-paru', '2019-01-01', 1, null, null, false),
(4, 4, 'Dokter THT', '2018-01-01', 1, null, null, false),
(4, 1, 'Dokter THT', '2019-01-01', 1, null, null, false),
(4, 5, 'Dokter THT', '2019-01-01', 1, null, null, false),
(4, 5, 'Dokter THT', '2017-01-01', 1, null, null, false),
(5, 3, 'Dokter Mata', '2019-01-01', 1, null, null, false),
(5, 5, 'Dokter Mata', '2019-01-01', 1, null, null, false),
(5, 5, 'Dokter Mata', '2018-01-01', 1, null, null, false),
(5, 1, 'Dokter Mata', '2018-01-01', 1, null, null, false),
(6, 4, 'Dokter Anak', '2015-01-01', 1, '2018-01-01', 1, true),
(6, 2, 'Dokter Anak', '2019-01-01', 1, null, null, false),
(6, 3, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(6, 5, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(7, 4, 'Dokter Anak', '2015-01-01', 1, '2018-01-01', 1, true),
(7, 2, 'Dokter Anak', '2019-01-01', 1, null, null, false),
(7, 3, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(7, 5, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(8, 4, 'Dokter Anak', '2015-01-01', 1, '2018-01-01', 1, true),
(8, 2, 'Dokter Anak', '2019-01-01', 1, null, null, false),
(8, 3, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(8, 5, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(9, 4, 'Dokter Anak', '2015-01-01', 1, '2018-01-01', 1, true),
(9, 2, 'Dokter Anak', '2019-01-01', 1, null, null, false),
(9, 3, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(9, 5, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(10, 4, 'Dokter Anak', '2015-01-01', 1, '2018-01-01', 1, true),
(10, 2, 'Dokter Anak', '2019-01-01', 1, null, null, false),
(10, 3, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(10, 5, 'Dokter Anak', '2018-01-01', 1, null, null, false),
(11, 3, 'Dokter Kandungan', '2016-01-01', 1, '2018-01-01', 1, true),
(11, 2, 'Dokter Kandungan', '2018-01-01', 1, null, null, false),
(11, 1, 'Dokter Kandungan', '2019-01-01', 1, null, null, false),
(11, 5, 'Dokter Kandungan', '2018-01-01', 1, null, null, false),
(12, 3, 'Dokter Kandungan', '2016-01-01', 1, '2018-01-01', 1, true),
(12, 2, 'Dokter Kandungan', '2018-01-01', 1, null, null, false),
(12, 1, 'Dokter Kandungan', '2019-01-01', 1, null, null, false),
(12, 5, 'Dokter Kandungan', '2018-01-01', 1, null, null, false);

--m_medical_facility_schedule
insert into m_medical_facility_schedule (medical_facility_id, day, time_schedule_start, time_schedule_end, created_by, created_on, is_delete)
values 
(1, 'Senin', '08:00', '17:00', 1, now(), false),
(1, 'Kamis', '08:00', '17:00', 1, now(), false),
(1, 'Jumat', '08:00', '17:00', 1, now(), false),
(2, 'Senin', '08:00', '17:00', 1, now(), false),
(2, 'Selasa', '08:00', '17:00', 1, now(), false),
(2, 'Rabu', '08:00', '17:00', 1, now(), false),
(3, 'Senin', '08:00', '17:00', 1, now(), false),
(3, 'Kamis', '08:00', '17:00', 1, now(), false),
(3, 'Sabtu', '08:00', '17:00', 1, now(), false),
(4, 'Rabu', '08:00', '17:00', 1, now(), false),--inijadwalonline
(5, 'Rabu', '00:10', '20:00', 1, now(), false),
(5, 'Kamis', '11:00', '23:00', 1, now(), false),
(5, 'Sabtu', '11:00', '17:00', 1, now(), false);

--t_doctor_office_schedule
insert into t_doctor_office_schedule (doctor_id, medical_facility_schedule_id, created_on, created_by, deleted_on, deleted_by, is_delete)
values 
(1, 9, '2016-01-01', 1, '2018-01-01', 1, true),
(1, 1, '2018-01-01', 1, null, null, false),
(1, 5, '2019-01-01', 1, null, null, false),
(2, 9, '2013-01-01', 1, null, null, false),
(2, 2, '2019-01-01', 1, null, null, false),
(3, 5, '2019-01-01', 1, null, null, false),
(3, 7, '2020-01-01', 1, null, null, false),
(4, 1, '2018-01-01', 1, null, null, false),
(4, 10, '2019-01-01', 1, null, null, false),
(5, 9, '2018-01-01', 1, null, null, false),
(5, 2, '2020-01-01', 1, null, null, false),--inijadwalOnline
(1, 12, '2016-01-01', 1, '2018-01-01', 1, true),
(1, 11, '2018-01-01', 1, null, null, false),
(1, 11, '2019-01-01', 1, null, null, false),
(2, 12, '2013-01-01', 1, null, null, false),
(3, 11, '2019-01-01', 1, null, null, false),
(3, 11, '2020-01-01', 1, null, null, false),
(4, 12, '2018-01-01', 1, null, null, false),
(5, 12, '2018-01-01', 1, null, null, false),
(5, 12, '2020-01-01', 1, null, null, false),
(6, 9, '2015-01-01', 1, null, null, false),
(6, 2, '2019-01-01', 1, null, null, false),
(6, 11, '2015-01-01', 1, null, null, false),
(7, 9, '2017-01-01', 1, null, null, false),
(7, 2, '2019-01-01', 1, null, null, false),
(7, 11, '2017-01-01', 1, null, null, false),
(8, 9, '2017-01-01', 1, null, null, false),
(8, 2, '2019-01-01', 1, null, null, false),
(8, 11, '2017-01-01', 1, null, null, false),
(9, 9, '2017-01-01', 1, null, null, false),
(9, 2, '2019-01-01', 1, null, null, false),
(9, 11, '2017-01-01', 1, null, null, false),
(10, 9, '2017-01-01', 1, null, null, false),
(10, 2, '2019-01-01', 1, null, null, false),
(10, 11, '2017-01-01', 1, null, null, false),
(11, 9, '2016-01-01', 1, '2018-01-01', 1, true),
(11, 1, '2018-01-01', 1, null, null, false),
(11, 5, '2019-01-01', 1, null, null, false),
(11, 11, '2018-01-01', 1, null, null, false),
(12, 9, '2016-01-01', 1, '2018-01-01', 1, true),
(12, 1, '2018-01-01', 1, null, null, false),
(12, 5, '2019-01-01', 1, null, null, false),
(12, 11, '2018-01-01', 1, null, null, false);

--t_doctor_office_treatment
insert into t_doctor_office_treatment (created_by, created_on, doctor_treatment_id,doctor_office_id) 
values 
(1, now(), 1, 1),
(1, now(), 1, 2),
(1, now(), 1, 3),
(1, now(), 2, 5),
(1, now(), 2, 6),
(1, now(), 3, 7),
(1, now(), 3, 8),--iniLayananOnline
(1, now(), 1, 9),
(1, now(), 1, 10),
(1, now(), 1, 11),
(1, now(), 2, 12),
(1, now(), 2, 13),
(1, now(), 3, 14),
(1, now(), 3, 15),
(1, now(), 1, 16),
(1, now(), 1, 17),
(1, now(), 1, 18),
(1, now(), 2, 19),
(1, now(), 2, 20),
(1, now(), 3, 21),
(1, now(), 3, 22),
(1, now(), 1, 23),
(1, now(), 1, 24),
(1, now(), 1, 4),
(1, now(), 3, 25),
(1, now(), 1, 26),
(1, now(), 1, 27),
(1, now(), 3, 28),
(1, now(), 1, 29),
(1, now(), 1, 30),
(1, now(), 3, 31),
(1, now(), 1, 32),
(1, now(), 1, 33),
(1, now(), 3, 34),
(1, now(), 1, 35),
(1, now(), 1, 36),
(1, now(), 3, 37),
(1, now(), 1, 38),
(1, now(), 1, 39),
(1, now(), 3, 40),
(1, now(), 1, 41),
(1, now(), 1, 42),
(1, now(), 3, 43),
(1, now(), 1, 44),
(1, now(), 1, 45),
(1, now(), 1, 46),
(1, now(), 1, 47),
(1, now(), 1, 48),
(1, now(), 1, 49),
(1, now(), 1, 50),
(1, now(), 1, 51);

--t_appointment
insert into t_appointment (customer_id, doctor_office_id, doctor_office_schedule_id, doctor_office_treatment_id, created_by, created_on, is_delete)
values 
(1, 1, 1, 1, 1, now(), false),
(2, 1, 1, 1, 1, now(), false),
(3, 1, 1, 1, 1, now(), false),
(1, 2, 1, 1, 1, now(), false),
(2, 2, 1, 1, 1, now(), false),
(2, 3, 1, 1, 1, now(), false),
(3, 4, 1, 1, 1, now(), false),
(1, 5, 1, 1, 1, now(), false),
(2, 6, 1, 1, 1, now(), false),
(2, 6, 1, 1, 1, now(), false),
(2, 7, 1, 1, 1, now(), false),
(2, 7, 1, 1, 1, now(), false),
(2, 8, 1, 1, 1, now(), false),
(2, 8, 1, 1, 1, now(), false),
(2, 9, 1, 1, 1, now(), false),
(2, 9, 1, 1, 1, now(), false),
(2, 10, 1, 1, 1, now(), false),
(2, 10, 1, 1, 1, now(), false),
(2, 11, 1, 1, 1, now(), false),
(2, 11, 1, 1, 1, now(), false);

--m_blood_group
insert into m_blood_group (code, description, created_by, created_on, is_delete)
values ('AB', 'golongan darah AB', 1, now(), false),
('A', 'golongan darah A', 1, now(), false),
('B', 'golongan darah B', 1, now(), false),
('O', 'golongan darah O', 1, now(), false);

--m_customer
insert into m_customer (biodata_id, dob, gender, blood_group_id, rhesus_type, height, weight, created_by, created_on, is_delete)
values (6, '118-07-10', 'L', 4, 'ABO', 173, 56, 1, now(), false),
(7, '1989-09-10', 'L', 2, 'ABO', 178, 76, 1, now(), false),
(8, '1967-10-10', 'L', 1, 'ABO', 170, 60, 1, now(), false);

--t_customer_chat
insert into t_customer_chat (customer_id, doctor_id, created_by, created_on, is_delete)
values (1, 1, 1, now(), false),
(2, 1, 1, now(), false),
(3, 1, 1, now(), false),
(1, 2, 1, now(), false),
(2, 3, 1, now(), false);

insert into t_doctor_office_treatment_price 
(created_by, created_on, doctor_office_treatment_id,price,price_start_from,price_until_from) values
(1, now(), 1,300000.0,300000.0,500000.0),
(1, now(), 2,300000.0,300000.0,500000.0),
(1, now(), 3,300000.0,300000.0,500000.0),
(1, now(), 4,300000.0,300000.0,500000.0),
(1, now(), 5,300000.0,300000.0,500000.0),
(1, now(), 6,300000.0,300000.0,500000.0),
(1, now(), 7,300000.0,300000.0,500000.0),
(1, now(), 9,0,0,0),
(1, now(), 10,0,0,0),
(1, now(), 11,0,0,0),
(1, now(), 12,0,0,0),
(1, now(), 13,0,0,0),
(1, now(), 14,300000.0,300000.0,500000.0),
(1, now(), 15,300000.0,300000.0,500000.0),
(1, now(), 16,300000.0,300000.0,500000.0),
(1, now(), 17,300000.0,300000.0,500000.0),
(1, now(), 18,300000.0,300000.0,500000.0),
(1, now(), 19,300000.0,300000.0,500000.0),
(1, now(), 20,300000.0,300000.0,500000.0),
(1, now(), 21,300000.0,300000.0,500000.0),
(1, now(), 22,300000.0,300000.0,500000.0),
(1, now(), 23,300000.0,300000.0,500000.0),
(1, now(), 24,300000.0,300000.0,500000.0),
(1, now(), 25,300000.0,300000.0,500000.0),
(1, now(), 26,300000.0,300000.0,500000.0),
(1, now(), 27,300000.0,300000.0,500000.0),
(1, now(), 28,300000.0,300000.0,500000.0),
(1, now(), 29,300000.0,300000.0,500000.0),
(1, now(), 30,300000.0,300000.0,500000.0),
(1, now(), 31,300000.0,300000.0,500000.0),
(1, now(), 32,300000.0,300000.0,500000.0),
(1, now(), 33,300000.0,300000.0,500000.0),
(1, now(), 34,300000.0,300000.0,500000.0),
(1, now(), 35,300000.0,300000.0,500000.0),
(1, now(), 36,300000.0,300000.0,500000.0),
(1, now(), 37,300000.0,300000.0,500000.0),
(1, now(), 38,300000.0,300000.0,500000.0),
(1, now(), 39,300000.0,300000.0,500000.0),
(1, now(), 40,300000.0,300000.0,500000.0),
(1, now(), 41,300000.0,300000.0,500000.0),
(1, now(), 42,300000.0,300000.0,500000.0),
(1, now(), 43,300000.0,300000.0,500000.0),
(1, now(), 44,300000.0,300000.0,500000.0),
(1, now(), 45,300000.0,300000.0,500000.0),
(1, now(), 46,300000.0,300000.0,500000.0),
(1, now(), 47,300000.0,300000.0,500000.0),
(1, now(), 48,300000.0,300000.0,500000.0),
(1, now(), 49,300000.0,300000.0,500000.0),
(1, now(), 50,300000.0,300000.0,500000.0),
(1, now(), 51,300000.0,300000.0,500000.0);
