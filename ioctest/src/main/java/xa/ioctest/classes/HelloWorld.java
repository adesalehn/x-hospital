package xa.ioctest.classes;

public class HelloWorld {
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public void getMessage() {
    	String x = message;
    	String y = "";
    	
    	for(int i = x.length()-1; i >= 0; i--) {
    		y += x.charAt(i);
    	}
    	if(y.equals(x)) {
    		System.out.println(x + " Is Palindrome");
    	}
    	else {
    		 System.out.println(x + " Is not Palindrome");
    	}
    		
       
    }
}
