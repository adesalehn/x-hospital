package xa.ioctest.configurations;

import java.util.Scanner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import xa.ioctest.classes.HelloWorld;

@Configuration
public class Application {
    @Bean
    public HelloWorld palincdromeChecker() {
        HelloWorld palincdromeCheck = new HelloWorld();
        Scanner x = new Scanner(System.in);
        System.out.print("Palindrome checker! Insert word : "); String input = x.nextLine();
        palincdromeCheck.setMessage(input);
        x.close();
        return palincdromeCheck;
        
    }
}
