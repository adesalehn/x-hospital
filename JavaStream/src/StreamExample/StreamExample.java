package StreamExample;

import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class StreamExample {

    public static void main(String[] args) {

        List < String > programmingLanguages = new ArrayList < > ();
        programmingLanguages.add("C");
        programmingLanguages.add("C++");
        programmingLanguages.add("Java");
        programmingLanguages.add("Kotlin");
        programmingLanguages.add("Python");
        programmingLanguages.add("Perl");
        programmingLanguages.add("Ruby");

        // filter all four character words into a list
        String joined = programmingLanguages.stream().collect(Collectors.joining(","));

        System.out.printf("Joined string: %s", joined);
    }
}