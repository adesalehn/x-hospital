package StreamExample;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class DemoStream {
	
    public static void main(String[] args) throws IOException {

        Collection<String> collection = Arrays.asList("JAVA", "J2EE", "Spring", "Hibernate");
        Stream<String> stream2 = collection.stream();
        stream2.forEach(System.out::println);

        List<String> list = Arrays.asList("JAVA", "J2EE", "Spring", "Hibernate");
        Stream<String> stream3 = list.stream();
        stream3.forEach(System.out::println);

        Set<String> set = new HashSet<>(list);
        Stream<String> stream4 = set.stream();
        stream4.forEach(System.out::println);
    }
    
    public static void main1(String[] args) throws IOException {

        // Array can also be a source of a Stream
        Stream<String> streamOfArray = Stream.of("a", "b", "c");
        streamOfArray.forEach(System.out::println);

        // creating from existing array or of a part of an array:
        String[] arr = new String[] { "a", "b", "c" };
        Stream<String> streamOfArrayFull = Arrays.stream(arr);
        streamOfArrayFull.forEach(System.out::println);

        Stream<String> streamOfArrayPart = Arrays.stream(arr, 1, 3);
        streamOfArrayPart.forEach(System.out::println);
    }
    
    public class Product {
    	
        private int id;
        private String name;
        private float price;
        public Product(int id, String name, float price) {
            this.id = id;
            this.name = name;
            this.price = price;
        }
        public int getId() {
            return id;
        }
        public void setId(int id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public float getPrice() {
            return price;
        }
        public void setPrice(float price) {
            this.price = price;
        }
    }
    
    /**
     * Filtering Collection without using Stream
     * @author Ramesh Fadatare
     *
     */
    
    public class JavaWithoutStreamExample {
        private List < Product > productsList = new ArrayList < Product > ();

        public void main(String[] args) {

            // Adding Products
            productsList.add(new Product(1, "HP Laptop", 25000f));
            productsList.add(new Product(2, "Dell Laptop", 30000f));
            productsList.add(new Product(3, "Lenevo Laptop", 28000f));
            productsList.add(new Product(4, "Sony Laptop", 28000f));
            productsList.add(new Product(5, "Apple Laptop", 90000f));
            // Without Java 8 Stream API'S
            withoutStreamAPI();
        }

        private void withoutStreamAPI() {
            // without Stream API's
            List < Float > productPriceList = new ArrayList < Float > ();
            // filtering data of list
            for (Product product: productsList) {
                if (product.getPrice() > 25000) {
                    // adding price to a productPriceList
                    productPriceList.add(product.getPrice());
                }
            }

            // displaying data
            for (Float price: productPriceList) {
                System.out.println(price);
            }
        }
    }
    
    /**
     * filtering Collection by using Stream
     * @author Ramesh Fadatare
     *
     */
    public class JavaStreamExample {
        private List < Product > productsList = new ArrayList < Product > ();

        public void main(String[] args) {

            // Adding Products
            productsList.add(new Product(1, "HP Laptop", 25000f));
            productsList.add(new Product(2, "Dell Laptop", 30000f));
            productsList.add(new Product(3, "Lenevo Laptop", 28000f));
            productsList.add(new Product(4, "Sony Laptop", 28000f));
            productsList.add(new Product(5, "Apple Laptop", 90000f));
            // With Java 8 Stream API'S
            withStreamAPI();
        }

        private void withStreamAPI() {
            // filtering data of list
            List < Float > productPriceList = productsList.stream().filter((product) -> product.getPrice() > 25000)
                .map((product) -> product.getPrice()).collect(Collectors.toList());
            // displaying data
            productPriceList.forEach((price) -> System.out.println(price));
        }
    }
    
    /**
     * Sum by using Collectors Methods
     * 
     * @author Ramesh Fadatare
     *
     */
    public class SumByUsingCollectorsMethods {
        public void main(String[] args) {
            List < Product > productsList = new ArrayList < Product > ();
            //Adding Products  
            productsList.add(new Product(1, "HP Laptop", 25000f));
            productsList.add(new Product(2, "Dell Laptop", 30000f));
            productsList.add(new Product(3, "Lenevo Laptop", 28000f));
            productsList.add(new Product(4, "Sony Laptop", 28000f));
            productsList.add(new Product(5, "Apple Laptop", 90000f));
            // Using Collectors's method to sum the prices.  
            double totalPrice3 = productsList.stream()
                .collect(Collectors.summingDouble(product -> product.getPrice()));
            System.out.println(totalPrice3);

        }
    }
    
    /**
     * Find Max and Min Product Price
     * 
     * @author Ramesh Fadatare
     *
     */
    public class FindMaxAndMinMethods {
        public void main(String[] args) {
            List < Product > productsList = new ArrayList < Product > ();
            // Adding Products
            productsList.add(new Product(1, "HP Laptop", 25000f));
            productsList.add(new Product(2, "Dell Laptop", 30000f));
            productsList.add(new Product(3, "Lenevo Laptop", 28000f));
            productsList.add(new Product(4, "Sony Laptop", 28000f));
            productsList.add(new Product(5, "Apple Laptop", 90000f));
            // max() method to get max Product price
            Product productA = productsList.stream()
                .max((product1, product2) -> product1.getPrice() > product2.getPrice() ? 1 : -1).get();

            System.out.println(productA.getPrice());
            // min() method to get min Product price
            Product productB = productsList.stream()
                .max((product1, product2) -> product1.getPrice() < product2.getPrice() ? 1 : -1).get();
            System.out.println(productB.getPrice());
        }
    }
    
    /**
     * Convert List into Set
     * 
     * @author Ramesh Fadatare
     *
     */
    public class ConvertListToSet {
        public void main(String[] args) {
            List < Product > productsList = new ArrayList < Product > ();

            // Adding Products
            productsList.add(new Product(1, "HP Laptop", 25000f));
            productsList.add(new Product(2, "Dell Laptop", 30000f));
            productsList.add(new Product(3, "Lenevo Laptop", 28000f));
            productsList.add(new Product(4, "Sony Laptop", 28000f));
            productsList.add(new Product(5, "Apple Laptop", 90000f));

            // Converting product List into Set
            Set < Float > productPriceList = productsList.stream().filter(product -> product.getPrice() < 30000)
                .map(product -> product.getPrice()).collect(Collectors.toSet());
            System.out.println(productPriceList);
        }
    }
    
    public class ConvertListToMap {
        public void main(String[] args) {
            List < Product > productsList = new ArrayList < Product > ();

            // Adding Products
            productsList.add(new Product(1, "HP Laptop", 25000f));
            productsList.add(new Product(2, "Dell Laptop", 30000f));
            productsList.add(new Product(3, "Lenevo Laptop", 28000f));
            productsList.add(new Product(4, "Sony Laptop", 28000f));
            productsList.add(new Product(5, "Apple Laptop", 90000f));

            // Converting Product List into a Map
            Map<Object, Object> productPriceMap = productsList.stream()
                .collect(Collectors.toMap(p -> p.getId(), p -> p.getName()));
            System.out.println(productPriceMap);
        }
    }
    
    public class MethodReferenceInStream {
        public void main(String[] args) {

            List < Product > productsList = new ArrayList < Product > ();

            // Adding Products
            productsList.add(new Product(1, "HP Laptop", 25000f));
            productsList.add(new Product(2, "Dell Laptop", 30000f));
            productsList.add(new Product(3, "Lenevo Laptop", 28000f));
            productsList.add(new Product(4, "Sony Laptop", 28000f));
            productsList.add(new Product(5, "Apple Laptop", 90000f));

            List < Float > productPriceList = productsList.stream()
                .filter(p -> p.getPrice() > 30000) // filtering data 
                .map(Product::getPrice) // fetching price by referring getPrice method
                .collect(Collectors.toList()); // collecting as list
            System.out.println(productPriceList);
        }
    }
}
